#include <assert.h>
#include <ctype.h>
#include "utilitarios.h"


void to_lower_case(char palavra[64]) {
    if (palavra == NULL) {
        return;
    }

    int i, j;
    for (i = 0; palavra[i] != '\0'; ++i) {
        while (!((palavra[i] >= 'a' && palavra[i] <= 'z') || (palavra[i] >= 'A' && palavra[i] <= 'Z') ||
                 palavra[i] == '\0')) {
            for (j = i; palavra[j] != '\0'; ++j) {
                palavra[j] = palavra[j + 1];
            }
            palavra[j] = '\0';
        }
        palavra[i] = tolower(palavra[i]);
    }
}

bool is_word_valid(string palavra) {

    if (strlen(palavra) < 2)
        return false;

    return true;
}

char **separa_string(char *termo, const char delimitador) {

    char **result = 0;
    size_t count = 0;
    char *temp = termo;
    char *ultimo_delim = 0;
    char delim[2];
    delim[0] = delimitador;
    delim[1] = 0;

    while (*temp) {
        if (delimitador == *temp) {
            count++;
            ultimo_delim = temp;
        }
        temp++;
    }

    count += ultimo_delim < (termo + strlen(termo) - 1);
    count++;

    result = malloc(sizeof(char *) * count);

    if (result) {
        size_t index = 0;
        char *token = strtok(termo, delim);

        while (token) {
            assert(index < count);
            *(result + index++) = strdup(token);
            token = strtok(0, delim);
        }
        assert(index == count - 1);
        *(result + index) = 0;
    }

    return result;
}


double calcula_TF(int quantidade, int total_palavras) {
    if (total_palavras == 0)
        return 0;

    return ((double) quantidade / (double) total_palavras);

}

double calcula_IDF(int total_arquivos, int total_arquivos_validos) {

    if (total_arquivos_validos == 0)
        return 0;

    double x = ((double) (total_arquivos / total_arquivos_validos));

    return log10(x);

}

double calcula_TFIDF(int quantidade, int total_palavras, int total_arquivos, int total_arquivos_validos) {
    double tf = calcula_TF(quantidade, total_palavras);
    double idf = calcula_IDF(total_arquivos, total_arquivos_validos);

    return (tf * idf);
}
