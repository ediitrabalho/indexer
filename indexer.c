/*
Feito Por:
...
Luiz Felipe Tozati
Igor Nathan Lobato
Victor Lara Silva
...
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "trie.h"
#include "utilitarios.h"

// Cores
#define GREEN "\x1B[32m"
#define YELLOW "\x1B[33m"
#define RED "\x1B[31m"
#define RESET "\x1B[0m"


void imprimir_colorido(double tempo_gasto) {
    printf("\n");
    if (tempo_gasto <= 30.0) {
        // Até 30 segundos verde
        printf("%sTempo de execução: %f segundos%s\n\n", GREEN, tempo_gasto, RESET);
    } else if (tempo_gasto <= 60.0) {
        // Até 1 minuto amarelo
        printf("%sTempo de execução: %f segundos%s\n\n", YELLOW, tempo_gasto, RESET);
    } else {
        // Mais de 1 minuto vermelho
        printf("%sTempo de execução: %f segundos%s\n\n", RED, tempo_gasto, RESET);
    }
}


void freq(string vetor_args[]) {
    clock_t start_time, end_time;
    start_time = clock();
    
    node *trie = novo_no();
    int quantidade = strtol(vetor_args[2], NULL, 10);
    Palavras *ranking = (Palavras *) malloc(sizeof(Palavras) * (quantidade));
    processa_arquivo(vetor_args[3], trie, quantidade, ranking);
    printf("\nOs %d termos mais frequentes no arquivo %s são:\n", quantidade, vetor_args[3]);

    for (int i = 0; i < quantidade; i++) {
        printf("%2dº | %5s | %4.0f vezes\n", i + 1, ranking[i].word, (ranking[i].count));
    }
    
    end_time = clock();
    double tempo_gasto = ((double)(end_time - start_time)) / CLOCKS_PER_SEC;
    imprimir_colorido(tempo_gasto);
}



void freq_word(string arquivo, string busca) {
    clock_t start_time, end_time;
    start_time = clock();

    int quantidade;
    node *trie = novo_no();

    processa_arquivo(arquivo, trie, 0, NULL);

    if (conta_palavra(busca, trie, &quantidade))
        printf("A palavra %s foi encontrada %d vezes no arquivo.\n", busca, quantidade);
    else
        printf("A palavra %s não foi encontrada no arquivo.\n", busca);

    end_time = clock();
    double tempo_gasto = ((double) (end_time - start_time)) / CLOCKS_PER_SEC;
    imprimir_colorido(tempo_gasto);
}

void search(int cont_args, string vetor_args[]) {
    clock_t start_time, end_time;
    start_time = clock();

    int quantidade_arquivos = (cont_args - 3);
    int quantidade_termos = 0;
    node **trie_busca = malloc(sizeof(node) * quantidade_arquivos);

    Palavras *relevancia;
    relevancia = (Palavras *) malloc(sizeof(Palavras) * (quantidade_arquivos));

    char **termos;
    termos = separa_string(vetor_args[2], ' ');
    for (int i = 0; *(termos + i); i++)
        quantidade_termos++;

    int palavras_no_termo = 1;
    for (int i = 0; *(termos + i); i++)
        palavras_no_termo++;

    int **arquivos_validos = malloc(sizeof(int *) * quantidade_arquivos);

    for (int i = 0; i < quantidade_arquivos + 2; i++)
        arquivos_validos[i] = malloc(sizeof(int *) * palavras_no_termo);

    for (int i = 0; i < quantidade_arquivos; i++) {
        trie_busca[i] = novo_no();
        processa_arquivo(vetor_args[i + 3], trie_busca[i], 0, NULL);

        if (termos != NULL) {
            for (int j = 0; *(termos + j); j++) {
                int valor;
                conta_palavra(*(termos + j), trie_busca[i], &valor);
                if (valor > 0) {
                    arquivos_validos[i][j]++;
                }
            }
        }

        strcpy(relevancia[i].word, vetor_args[i + 3]);
    }

    for (int i = 0; i < quantidade_arquivos; i++) {
        int quantidade_palavras = conta_total(trie_busca[i]);
        relevancia[i].count = 0;

        if (termos) {
            for (int j = 0; *(termos + j); j++) {
                int count = trie_count(trie_busca[i], *(termos + j));
                relevancia[i].count += calcula_TFIDF(count, quantidade_palavras, quantidade_arquivos, arquivos_validos[i][j]);
            }
        }
        relevancia[i].count = ((double) relevancia[i].count / quantidade_termos) * 100;
    }

    printf("\n");
    for (int i = 0; i < quantidade_arquivos; i++) {
        printf("O arquivo %s é %f%% relevante segundo TFIDF\n", relevancia[i].word, relevancia[i].count);
    }

    end_time = clock();
    double tempo_gasto = ((double) (end_time - start_time)) / CLOCKS_PER_SEC;
    imprimir_colorido(tempo_gasto);
}

int main(int argc, string argv[]) {
    if (argc < 4 || strcmp(argv[1], "--help") == 0) {
        printf("-----Indexer-----\n\n");
        printf("Realiza a indexação de palavras em documentos de texto");

        printf("\nESPECIFICAÇÕES:\n");
        printf("O programa **indexer** realiza uma contagem de palavras em documentos de texto. A partir dessa contagem, ele ainda permite uma busca pelo número de ocorrências de uma palavra específica em um documento, ou a seleção de documentos relevantes para um dado termo de busca.\nO programa **indexer** transforma todas as letras para minúsculas e ignora caracteres como números e pontuações.\nQuando executado com a opção --freq, o programa **indexer** irá exibir o número de ocorrências das N palavras mais frequentes no documento passado como parâmetro, em ordem decrescente de ocorrências.\nQuando executado com a opção --freq-word, o programa **indexer** exibe a contagem de uma palavra específica no documento passado como parâmetro.\nPor fim, quando executado com a opção --search, o programa **indexer** apresenta uma listagem dos documentos mais relevantes para um dado termo de busca. Para tanto, o programa utiliza o cálculo TF-IDF (Term Frequency-Inverse Document Frequency).");

        printf("\nOPÇÕES:\n");
        printf("   --freq N ARQUIVO                         Exibe o número de ocorrência das N palavras que mais aparecem em ARQUIVO, em ordem decrescente de ocorrência.\n");
        printf("   --freq-word PALAVRA ARQUIVO              Exibe o número de ocorrências de PALAVRA em ARQUIVO. \n");
        printf("   --search TERMO ARQUIVO [ARQUIVO ...]     Exibe uma listagem dos ARQUIVOS mais relevantes encontrados pela busca por TERMO. A listagem é apresentada em ordem descrescente de relevância. TERMO pode conter mais de uma palavra. Neste caso, deve ser indicado entre àspas.\n");

    } else if (strcmp(argv[1], "--freq") == 0) {
        freq(argv);

    } else if (strcmp(argv[1], "--freq-word") == 0) {
        freq_word(argv[3], argv[2]);

    } else if (strcmp(argv[1], "--search") == 0) {
        search(argc, argv);

    } else {
        printf("Houve um erro na passagem de parâmetros. O comando ./main --help pode ajudá-lo a executar o programa corretamente.\n");
    }
}


