#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#ifndef INDEXER_UTILS_H
#define INDEXER_UTILS_H

#endif

typedef char *string;

void to_lower_case(char palavra[64]);

bool is_word_valid(string palavra);

char **separa_string(char *termo, const char delimitador);

double calcula_TF(int qtd, int total_palavras);

double calcula_IDF(int total_arquivos, int total_arquivos_com_palavras);

double calcula_TFIDF(int qtd, int total_palavras, int total_arquivos, int arquivos_com_palavras);

