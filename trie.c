#include "trie.h"

#define convertToIndex(c) ((int)c - (int)'a')

struct node *novo_no(void) {

    node *pNode = malloc(sizeof(node));
    if (pNode) {
        pNode->is_final_word = false;
        memset(pNode->children, 0, sizeof(pNode->children));
    }
    return pNode;
}

void inserir(string palavra, node *trie) {

    node *no_trie = trie;

    for (const char *p = palavra; *p; p++) {
        int index = convertToIndex(*p);
        if (!no_trie->children[index])
            no_trie->children[index] = novo_no();

        no_trie = no_trie->children[index];
    }


    no_trie->is_final_word = true;
    no_trie->count++;

}

bool conta_palavra(string palavra, node *trie, int *repete) {
    int altura, tamanho_palavra = strlen(palavra), i;

    node *no_trie = trie;
    for (altura = 0; altura < tamanho_palavra; altura++) {
        i = convertToIndex(palavra[altura]);

        if (!no_trie->children[i])
            return false;

        no_trie = no_trie->children[i];
    }
    *repete = no_trie->count;

    return (no_trie->is_final_word);
}

int trie_count(node *trie_busca, char *termo) {
    if (trie_busca == NULL)
        return 0;

    node *atual = trie_busca;

    while (*termo) {
        atual = atual->children[*termo - 'a'];

        if (atual == NULL)
            return 0;

        termo++;
    }

    return atual->count;
}


void processa_arquivo(string nome, node *trie_busca, int n, Palavras *palavras) {
    FILE *arquivo = fopen(nome, "r");
    if (arquivo == NULL) {
        fprintf(stderr, "Erro ao abrir o arquivo '%s'.\n", nome);
        return;
    }

    char buffer[512], b_aux[512];
    int aux;

    printf("Processando o arquivo %s...\n", nome);

    while (fscanf(arquivo, "%s", buffer) != EOF) {
        to_lower_case(buffer);

        if (!is_word_valid(buffer))
            continue;

        inserir(buffer, trie_busca);

        int count = trie_count(trie_busca, buffer);
        for (int i = 0; i < n; i++) {
            if (count > palavras[i].count) {

                if (i > 0 && strcmp(palavras[i-1].word, buffer) == 0)
                    break;

                aux = palavras[i].count;
                strcpy(b_aux, palavras[i].word);

                palavras[i].count = count;
                strcpy(palavras[i].word, buffer);

                if (strcmp(b_aux, buffer) == 0)
                    break;

                for (int j = i+1; j < n; j++){
                    if (strcmp(palavras[j].word, buffer) == 0)
                        palavras[j].count = -1;
                }

                count = aux;
                strcpy(buffer, b_aux);
            }
        }
    }
    fclose(arquivo);
}

int conta_total(node *top) {
    int result = 0;

    if (top->is_final_word)
        result += top->count;

    for (int i = 0; i < 26; i++)
        if (top->children[i])
            result += conta_total(top->children[i]);

    return result;
}

