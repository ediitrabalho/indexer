# Indexer
   Projeto de um indexador de palavras de arquivos de texto utilizando a estrutura de dados Trie.

## Trie (Árvore de Prefixos):

- **Definição:** A Trie, ou árvore de prefixos, é uma estrutura arborescente para armazenar e pesquisar conjuntos dinâmicos de strings ou chaves.

- **Características Principais:**
  - Cada nó representa um caractere, formando relacionamentos com base nos caracteres das strings.
  - Mantém um prefixo único para cada string armazenada.
  
- **Funcionamento:**
  - Inserção ocorre caractere por caractere, formando caminhos distintos na árvore.
  - Busca eficiente em tempo linear em relação ao comprimento da string pesquisada.
  
- **Vantagens:**
  - Recuperação rápida de prefixos comuns.
  - Eficiência em operações de busca de strings comparada a outras estruturas de dados.


### Descrição
  * O programa **indexer** realiza uma contagem de palavras em documentos de 
  texto. A partir dessa contagem, ele ainda permite uma busca pelo número de 
  ocorrências de uma palavra específica em um documento, ou a seleção de 
  documentos relevantes para um dado termo de busca.
  O programa **indexer** transforma todas as letras para minúsculas e ignora
  caracteres como números e pontuações.
  Quando executado com a opção --freq, o programa **indexer** irá exibir o 
  número de ocorrências das N palavras mais frequentes no documento passado 
  como parâmetro, em ordem decrescente de ocorrências.
  Quando executado com a opção --freq-word, o programa **indexer** exibe a 
  contagem de uma palavra específica no documento passado como parâmetro.
  Por fim, quando executado com a opção --search, o programa **indexer** 
  apresenta uma listagem dos documentos mais relevantes para um dado termo de 
  busca. Para tanto, o programa utiliza o cálculo TF-IDF (Term 
  Frequency-Inverse Document Frequency).

### Opções
  * **--freq N ARQUIVO**
    Exibe o número de ocorrência das N palavras que mais aparecem em ARQUIVO, em
    ordem decrescente de ocorrência.
  * **--freq-word PALAVRA ARQUIVO**
    Exibe o número de ocorrências de PALAVRA em ARQUIVO. 
  * **--search TERMO ARQUIVO [ARQUIVO ...]**
    Exibe uma listagem dos ARQUIVOS mais relevantes encontrados pela busca por 
    TERMO. A listagem é apresentada em ordem descrescente de relevância. 
    TERMO pode conter mais de uma palavra. Neste caso, deve ser indicado entre 
    àspas.

## Funcionamento

Realizar o clone do repositório através do SSH ou HTTPS:

  * SSH:
  ```
  $ git clone git@gitlab.com:ediitrabalho/indexer.git
  ```
  * HTTPS:
  ```
  $ git clone https://gitlab.com/ediitrabalho/indexer.git

  ```

  Acessar o diretório do projeto através do terminal:
  ```
  $ cd indexer
  ```
  Compilar o projeto:
  ```
  $ gcc indexer.c trie.c utilitarios.c -o indexer -lm
  ```
  Escolher algum arquivo de texto disponibilizado para teste:
  ```
  $ ls
  ```

  Executar o indexer com o arquivo escolhido. 
  
  ```
  $ ./indexer --freq 5 101.txt
  $ ./indexer --freq-word copyright 101.txt
  $ ./indexer --search "the of and to in" 1015.txt 1020.txt
  ```
### Exemplos de uso:
### --freq
  ![](./imgs/01.png)

  ![](./imgs/03.png)

  ![](./imgs/04.png)

  ![](./imgs/2gb.png)

  ![](./imgs/4gb.png)

  ![](./imgs/6gb.png)

  ![](./imgs/12gb.png)

---
### --freq-word
  ![](./imgs/02.png)

  ![](./imgs/08.png)
---
### --search
  ![](./imgs/05.png)

  ![](./imgs/06.png)

  ![](./imgs/07.png)
  

