#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include "utilitarios.h"

typedef char *string;

typedef struct node {
    struct node *children[26];
    int count;
    bool is_final_word;
} node;

typedef struct Palavras {
    double count;
    char word[64];
} Palavras;

node *novo_no(void);

void inserir(string palavra, node *trie);

bool conta_palavra(string palavra, node *trie, int *repete);

int trie_count(node *trie_busca, char *termo);

void processa_arquivo(string nome, node *trie_busca, int n, Palavras *palavras);

int conta_total(node *top);
